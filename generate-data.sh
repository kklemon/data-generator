#!/bin/bash

OUTPUT=/home/kristian/Development/deep-learning/training-data

TRAIN=20000
VALID=2000

if [ -d "$OUTPUT/data" ]; then
	rm -r $OUTPUT/data
fi

bin/data-generator -absolute=true -imagesize=64 -out=$OUTPUT -train=$TRAIN -valid=$VALID
