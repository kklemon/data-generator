# Data generator

Generate images with simple shapes for feeding Deep Learning networks.

## Why?

Deep Learning networks have to be feed with a really high number of training images to achieve good prediction results.

Creating data for specific tasks can be very costly sometimes, so i want to show that it is possible to use computer generated visual data for training that lead to same results as real world data would do it.

This is just a simple practicability test, and i want to extend this technique to other areas in future.

## Compilation

#### Install dependencies

You will nedd **Cmake**, **cairomm**, 'system' and 'filesystem' **Boost** modules

**Ubuntu**:

`sudo apt-get install libcairomm-1.0-dev libboost-filesystem-dev libboost-system-dev`

#### Compile

```
cmake .
make
```

## Usage

Generate 10K training images and 1K validation/test images:

```
./bin/data-generator -out FOLDER -imagesize 64 -train 10000 -valid 1000
```

or use `date-generator.sh`.

#### Output

The output will consists of two folders each with the training and validation images. In the parent folder text files with labels for the images and the label names will be created.

#### Training

To train a network in Caffe you have to put the output into a format Caffe can read, like a LMDB or LevelDB database. Use the scripts Caffe provides in the __tools__ folder or build your owns.

It may be necessary to subtract the mean from image set. Caffe provide also tools for this.
