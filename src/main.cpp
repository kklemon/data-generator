#include "CShapeDrawer.hpp"
#include "Color.hpp"

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <cmath>
#include <utility>
#include <fstream>
#include <thread>
#include <gflags/gflags.h>
#include <boost/filesystem.hpp>

namespace bfs = boost::filesystem;

DEFINE_int32(imagesize, 128, "The size of the generated images");
DEFINE_string(out, "", "Output folder");
DEFINE_int32(train, 100, "Number of training images to generate");
DEFINE_int32(valid, 100, "Number of validation images to generate");
DEFINE_bool(absolute, false, "Write image paths absolute");
DEFINE_bool(stroke, true, "Generate stroked shapes");

void generateImageSet(const std::string &setName, const std::string &folder, const int num) {
	if (num < 1) return;

	std::string imageDir = folder + "data/" + setName + "/";

	try {
		bfs::create_directory(imageDir);
	}
	catch (const bfs::filesystem_error &e) {
		std::cerr << "Failed to create directory: " << imageDir << std::endl;
		return;
	}

	CShapeDrawer drawer(FLAGS_imagesize, FLAGS_stroke);

	std::vector<std::pair<std::string, int> > set;

	// number of digits we need to store the highest image number
	int numLen = (int) (log10(num-1)+1);

	for (int i = 0; i < num; ++i) {
		std::string filename = "image_";

		std::string numStr = std::to_string(i);
		for (int a = 0; a < numLen-numStr.length(); ++a) {
			filename += '0';
		}
		filename += numStr + ".png";

		Shape shape = getRandomShape();
		drawer.createImage(shape);
		drawer.getSurface()->write_to_png(imageDir + filename);

		set.push_back(std::make_pair(filename, (int) shape));
	}

	bfs::path file(folder + "data/" + setName + "_list.txt");
	std::ofstream ofs(file.native());
	for (auto pair: set) {
		if (FLAGS_absolute) ofs << imageDir;
		ofs << pair.first << " " << pair.second << std::endl;
	}
	ofs.close();
}

int main(int argc, char * argv[]) {
	google::SetUsageMessage("Generate a set of random shapes and pattern for feeding neural networks");
	google::ParseCommandLineFlags(&argc, &argv, false);

	if (argc < 2 || FLAGS_out == "") {
		google::ShowUsageWithFlagsRestrict(argv[0], "main");
		return 1;
	}
	if (FLAGS_train < 0 || FLAGS_valid < 0) {
		std::cerr << "Number of train or validation images mustn't be negative" << std::endl;
		return 1;
	}
	if (FLAGS_imagesize < 0) {
		std::cerr << "Image size mustn't be negative" << std::endl;
		return 1;
	}

	std::srand(std::time(0));

	std::string baseDir = FLAGS_out;
	if (FLAGS_out.back() != '/') baseDir += '/';

	try {
		bfs::create_directory(baseDir + "data");
	}
	catch (const bfs::filesystem_error &e) {
		std::cerr << "Failed to create directories" << std::endl;
		return 1;
	}

	// write label names to file
	std::string labels = getShapeLabelNames();

	bfs::path file(baseDir + "data/labels.txt");
	std::ofstream ofs(file.native());
	ofs.write(labels.c_str(), labels.length());
	ofs.close();

	std::cout << "Generating images..." << std::endl;

	// generate and save train set
	std::thread train(generateImageSet, "train", baseDir, FLAGS_train);
	// validation set
	std::thread valid(generateImageSet, "valid", baseDir, FLAGS_valid);

	train.join();
	valid.join();

	std::cout << "Done!" << std::endl;

	return 0;
}
