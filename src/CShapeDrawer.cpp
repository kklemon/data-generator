#include "CShapeDrawer.hpp"

#include "util.hpp"

using namespace Cairo;
using namespace util;

/*
 * CShapeDrawer implementation
 */
CShapeDrawer::CShapeDrawer(int size, bool stroke)
	: stroke(stroke) {
	setSurfaceSize(size);
}

CShapeDrawer::~CShapeDrawer() {
}

int CShapeDrawer::getSurfaceSize() const {
	return size;
}

bool CShapeDrawer::isStroke() const {
	return stroke;
}

void CShapeDrawer::setSurfaceSize(int size) {
	this->size = size;

	surface = Cairo::ImageSurface::create(Cairo::Format::FORMAT_RGB24, size, size);
	ctx = Cairo::Context::create(surface);
}

void CShapeDrawer::setStroke(bool stroke) {
	this->stroke = stroke;
}

const Cairo::RefPtr<Cairo::ImageSurface> CShapeDrawer::getSurface() const {
	return surface;
}

const Image * CShapeDrawer::createImage(Shape shape) {
	ctx->save();
	ctx->scale(size, size);

	drawBackground();
	drawShape(shape);
	drawOverlay();

	ctx->restore();

	Image * image = new Image(size, size, (const char *) surface->get_data());
	return image;
}

void CShapeDrawer::applyRandomColor(double a) {
	Color(
		Color::HSV::randomHue(
			util::randRange<double>(0.5, 1.0),
			util::randRange<double>(0.5, 1.0)
		)
	).applyToCtx(ctx, a);
}

void CShapeDrawer::drawGradient(RefPtr<Gradient> &gr) {
	Color::HSV hsv = Color::HSV::randomHue(1, 1);
	Color(hsv).addStopColor(gr, 0, randRange<double>(0, 1));

	hsv.shiftHue(randRange<double>(-30, 30));
	Color(hsv).addStopColor(gr, randRange<double>(0.3, 0.6), randRange<double>(0, 1));

	hsv.shiftHue(randRange<double>(-30, 30));
	Color(hsv).addStopColor(gr, 1, randRange<double>(0, 1));

	ctx->set_source(gr);
	ctx->rectangle(0, 0, 1, 1);
	ctx->rotate(util::randRange<double>(0, 60));
	ctx->fill();
}

void CShapeDrawer::drawLinearPattern() {
	ctx->save();

	RefPtr<Gradient> gr = LinearGradient::create(
		randRange<double>(-0.2, 0.2), randRange<double>(-0.2, 0.2),
		randRange<double>(0.8, 1.2), randRange<double>(0.8, 1.2)
	);
	drawGradient(gr);

	ctx->restore();
}

void CShapeDrawer::drawRadialPattern() {
	ctx->save();

	RefPtr<Gradient> gr = RadialGradient::create(
		randRange<double>(-0.2, 0.2), randRange<double>(-0.2, 0.2), 0.1, //randRange<double>(0.1, 0.3),
		randRange<double>(0.8, 1.2), randRange<double>(0.8, 1.2), 0.4 //randRange<double>(0.4, 0.6)
	);
	drawGradient(gr);

	ctx->restore();
}

// generate some random pattern in the background
void CShapeDrawer::drawBackground() {
	bool linearPattern = true;
	bool radialPattern = std::rand() % 2;
	bool shapePattern = std::rand() % 2;
	bool linePattern = std::rand() % 2;

	ctx->save();

	// white background
	ctx->set_source_rgb(1.0, 1.0, 1.0);
	ctx->rectangle(0, 0, 1, 1);
	ctx->fill();

	// put a random color on it
	Color(Color::HSV::randomHue(1, 1)).applyToCtx(ctx);
	ctx->rectangle(0, 0, 1, 1);
	ctx->fill();

	if (linearPattern) drawLinearPattern();
	// TODO: fix radial pattern drawing
	//if (radialPattern) drawRadialPattern();

	// TODO: implement pattern drawing and other stuff

	ctx->restore();
}

void CShapeDrawer::drawShape(Shape shape) {
	ctx->save();
	Color(Color::HSV::random()).applyToCtx(ctx);

	bool stroke = stroke ? std::rand() % 3 == 0 : false;
	if (stroke) {
		ctx->set_line_width(util::randRange<double>(0.01, 0.1));
	}

	ctx->save();

	// size of the shape in percent of surface size
	// min: 0.15, max: 0:7
	double shapeWidth = util::randRange<double>(0.25, 0.8);
	double shapeHeight = shapeWidth;

	if (shape == Shape::RECTANGLE) {
		float factor = std::rand() % 2 == 0 ? 1.0 : -1.0;
		shapeHeight *= 1.0 + (factor * util::randRange<double>(0.4, 0.7));
	}
	else if (shape != Shape::SQUARE) {
		shapeHeight *= 1.0 + util::randRange<double>(-0.5, 0.5);
	}

	// a shape can be maximum 20% outside of the surface
	double outX = shapeWidth*0.1;
	double outY = shapeHeight*0.1;

	// calculate a random position within the area
	double posX = util::randRange<double>(-outX, 1.0-shapeWidth+outX);
	double posY = util::randRange<double>(-outY, 1.0-shapeHeight+outY);

	double rot = util::randRange<double>(-60, 60);

	ctx->translate(posX, posY);
	ctx->translate(0.5, 0.5);
	ctx->rotate_degrees(rot);
	ctx->translate(-0.5, -0.5);
		ctx->scale(shapeWidth, shapeHeight);

	switch (shape) {
		case CIRCLE: ctx->arc(0.5, 0.5, 0.5, 0, 360); break;
		case SQUARE: ctx->rectangle(0, 0, 1.0, 1.0); break;
		case RECTANGLE: ctx->rectangle(0, 0, 1.0, 1.0); break;
		case TRIANGLE:
			ctx->line_to(0.0, 1.0);
			ctx->line_to(1.0, 0.0);
			ctx->line_to(0.0, 0.0);
			ctx->line_to(0.0, 1.0);
			break;
	}

	if (stroke) {
		ctx->restore();
		ctx->stroke();
	}
	else {
		ctx->fill();
		ctx->restore();
	}

	ctx->restore();
}

void CShapeDrawer::drawOverlay() {

}
