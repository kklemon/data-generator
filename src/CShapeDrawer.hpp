#include "IShapeDrawer.hpp"
#include "Color.hpp"

#include <cairommconfig.h>
#include <cairomm/context.h>
#include <cairomm/surface.h>

class CShapeDrawer : public IShapeDrawer {
protected:
	CShapeDrawer();
public:
	CShapeDrawer(int size, bool stroke = true);
	~CShapeDrawer();

	int getSurfaceSize() const;
	bool isStroke() const;

	void setSurfaceSize(int size);
	void setStroke(bool stroke);

	const Cairo::RefPtr<Cairo::ImageSurface> getSurface() const;

	virtual const Image * createImage(Shape shape);

private:
	void applyRandomColor(double a = 1.0);

	void drawGradient(Cairo::RefPtr<Cairo::Gradient> &gr);
	void drawLinearPattern();
	void drawRadialPattern();

	void drawBackground();
	void drawShape(Shape shape);
	void drawOverlay();

	int size;
	bool stroke;

	Cairo::RefPtr<Cairo::Context> ctx;
	Cairo::RefPtr<Cairo::ImageSurface> surface;
};
