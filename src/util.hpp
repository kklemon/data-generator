#include <cstdlib>
#include <algorithm>

namespace util {
	template <typename T>
	T randRange(T min, T max) {
		return min + static_cast<T>(std::rand() / static_cast<T>(RAND_MAX / (max-min)));
	}

	template <typename T>
	T clamp(T value, T floor, T ceil) {
		return std::max(floor, std::min(value, ceil));
	}

	bool randBool();
}
