#include "IShapeDrawer.hpp"

#include <random>

Image::Image(int width, int height, const char * data)
	: width(width), height(height), data(data) {
}

Image::~Image() {
	if (data != nullptr)
		delete [] data;
}

int Image::getWidth() const {
	return width;
}

int Image::getHeight() const {
	return height;
}

const char * Image::getData() const {
	return data;
}

Shape getRandomShape() {
	return Shape(std::rand() % 4);
}

std::string getShapeLabelNames() {
	std::string names;

	names += "Circle\n";
	names += "Square\n";
	names += "Rectangle\n";
	names += "Triangle";

	return names;
}
