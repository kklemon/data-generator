set(SRC
	main.cpp
	util.cpp
	Color.cpp
	IShapeDrawer.cpp
	CShapeDrawer.cpp
)

add_executable(data-generator ${SRC})

# How to install and find this libs:
#  install cairomm-1.0-dev, libsigc++-2.0-dev and cairo-dev
#  Download the specific Find.cmake file for every library
#   and put them in the local or global cmake Modules dir

find_package(Cairomm REQUIRED)
find_package(GFlags REQUIRED)
find_package(Boost COMPONENTS filesystem system REQUIRED)
find_package(Threads REQUIRED)

if (Cairomm_FOUND)
	include_directories(${Cairomm_INCLUDE_DIRS})
	target_link_libraries(data-generator ${Cairomm_LIBRARIES})
endif (Cairomm_FOUND)

if (GFLAGS_FOUND)
	target_link_libraries(data-generator gflags)
endif (GFLAGS_FOUND)

if (Boost_FOUND)
	include_directories(${Boost_INCLUDE_DIRS})
	target_link_libraries(data-generator ${Boost_LIBRARIES})
endif (Boost_FOUND)

if (CMAKE_THREAD_LIBS_INIT)
  target_link_libraries(data-generator "${CMAKE_THREAD_LIBS_INIT}")
endif (CMAKE_THREAD_LIBS_INIT)
