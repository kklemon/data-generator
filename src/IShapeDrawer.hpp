/*
 * Interface for drawing shapes
 */

#include <string>

class Image {
private:
	int width, height;
	const char * data;

public:
	Image(int width, int height, const char * data);
	~Image();

	int getWidth() const;
	int getHeight() const;
	const char * getData() const;
};

enum Shape {
	CIRCLE, SQUARE, RECTANGLE, TRIANGLE
};

Shape getRandomShape();
std::string getShapeLabelNames();

class IShapeDrawer {
public:
	virtual ~IShapeDrawer() {};

	virtual const Image * createImage(Shape shape) = 0;
};
