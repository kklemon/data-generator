#ifndef COLOR_H
#define COLOR_H

#include <cairomm/context.h>
#include <iostream>

class Color {
public:
	// implement different color models
	struct RGB {
		static RGB random();

		double r, g, b;

		RGB(const Color &c);
		RGB(double r, double g, double b);

		void set(double r, double g, double b);
		void set(const Color &c);

		operator Color() const;
	};

	struct HSV {
		static HSV random();
		static HSV randomHue(double saturation, double value);

		double h, s, v;

		HSV(const Color &c);
		HSV(double h, double s, double v);

		void set(double h, double s, double v);
		void set(const Color &c);

		void shiftHue(double degrees);

		operator Color() const;
	};

	double r, g, b;

	Color(double r, double g, double b);
	Color(const Color &c);

	friend std::ostream& operator<<(std::ostream &out, const Color &c);

	void set(double r, double g, double b);
	void set(const Color &c);

	void applyToCtx(Cairo::RefPtr<Cairo::Context> &ctx, double a = 1.0) const;
	void addStopColor(Cairo::RefPtr<Cairo::Gradient> &gr, double offset, double a = 1.0) const;
};

#endif
