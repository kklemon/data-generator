#include "Color.hpp"

#include "util.hpp"

/*
 * Color::RGB implementation
 */
Color::RGB Color::RGB::random() {
	return Color::RGB((std::rand() % 256) / 255.0, (std::rand() % 256) / 255.0, (std::rand() % 256) / 255.0);
}

Color::RGB::RGB(const Color &c)
	: r(c.r), g(c.g), b(c.b) {
}

Color::RGB::RGB(double r, double g, double b)
	: r(r), g(g), b(b) {
}

void Color::RGB::set(double r, double g, double b) {
	this->r = r;
	this->g = g;
	this->b = b;
}

void Color::RGB::set(const Color &c) {
	this->r = c.r;
	this->g = c.g;
	this->b = c.b;
}

Color::RGB::operator Color() const {
	return Color(r, g, b);
}


/*
 * Color::HSV implementation
 */
Color::HSV Color::HSV::random() {
	return Color::HSV(util::randRange<double>(0, 360), (std::rand() % 256) / 255.0, (std::rand() % 256) / 255.0);
}

Color::HSV Color::HSV::randomHue(double saturation, double value) {
	return Color::HSV(util::randRange<double>(0, 360), saturation, value);
}

Color::HSV::HSV(const Color &c) {
	set(c);
}

Color::HSV::HSV(double h, double s, double v)
	: h(h), s(s), v(v) {
}

void Color::HSV::set(double h, double s, double v) {
	this->h = h;
	this->s = s;
	this->v = v;
}

void Color::HSV::set(const Color &c) {
	double max = std::max(c.r, std::max(c.g, c.b));
	double min = std::min(c.r, std::min(c.g, c.b));
	double d = max - min;

	if (d == 0)			 h = 0.0;
	else if (max == c.r) h = 60.0*((int) std::round((c.g-c.b)/d) % 6);
	else if (max == c.g) h = 60.0*(((c.b-c.r)/d) + 2);
	else if (max == c.b) h = 60.0*(((c.r-c.g)/d) + 4);

	if (max == 0) s = 0.0;
	else s = d/max;

	v = max;
}

void Color::HSV::shiftHue(double degrees) {
	h += degrees;
	if (h > 360) h -= 360;
	else if (h < 0) h += 360;
}

Color::HSV::operator Color() const {
	if (s == 0) return Color(v, v, v);

	int i;
	double f, p, q, t, sec;
	double r, g, b;

	sec = h/60.0;
	i = std::floor(sec);
	f = sec - i;
	p = v * (1-s);
	q = v * (1-s * f);
	t = v * (1-s * (1-f));


	switch (i) {
		case 0: return Color(v, t, p);
		case 1: return Color(q, v, p);
		case 2: return Color(p, v, t);
		case 3: return Color(p, q, v);
		case 4: return Color(t, p, v);
		default: return Color(v, p, q);
	}
}

Color::Color(double r, double g, double b)
	: r(r), g(g), b(b) {
}

Color::Color(const Color &c) {
	set(c);
}

std::ostream& operator<<(std::ostream &out, const Color &c) {
	return out << "[" << c.r << ", " << c.g << ", " << c.b << "]";
}

void Color::set(double r, double g, double b) {
	this->r = r;
	this->r = g;
	this->b = b;
}

void Color::set(const Color &c) {
	r = c.r;
	g = c.g;
	b = c.b;
}

void Color::applyToCtx(Cairo::RefPtr<Cairo::Context> &ctx, double a) const {
	ctx->set_source_rgba(r, g, b, a);
}

void Color::addStopColor(Cairo::RefPtr<Cairo::Gradient> &gr, double offset, double a) const {
	gr->add_color_stop_rgba(offset, r, g, b, a);
}
