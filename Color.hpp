#ifndef COLOR_H
#define COLOR_H

class Color {
public:
	Color(double r, double g, double b, double a);
	Color(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	Color(const Color &color);
	~Color();

};

#endif
